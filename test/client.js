const fs = require('fs');
/*const Client = require('../models/client.model');
const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://testUnitaireUser:testUnitairePassword@cluster0.0aykc.mongodb.net/$testUnitaireClients?retryWrites=true&w=majority`,
{   useNewUrlParser: true,
    useUnifiedTopology: true })
.then(() => console.log('Connexion à MongoDB réussie !'))
.catch(() => console.log('Connexion à MongoDB réussie !'));*/

exports.isFile = (filePath) => {
    try {
        return fs.existsSync(filePath)
    } catch(err) {
        return false;
    }
}

exports.isFileNamedProperly = (filePath) => {
    if (filePath.includes('client.txt')) {
        return true
    }
    return false
}

exports.isFileEmpty = (filePath) => {
    try{
        if (fs.readFileSync(filePath, 'utf8').length == 0)
            return true;
        return false;
    }catch(err){
        return true;
    }
}

exports.isDataCorrect = (filePath) => {

    let content_Tested = fs.readFileSync(filePath, 'utf8');

    if (content_Tested.length === 0){
      return false
    }

    const tabOfAllField = content_Tested.split(/[\n;]/);
    const tabOfAllLines = content_Tested.split(/[\n]/);
    const tabOfItems = [];

    for (let i = 0; i<tabOfAllLines.length; i++) {
        tabOfItems.push(tabOfAllLines[i].split(';'));
    }

    for(let i=0; tabOfItems.length>i; i++) {
        if(tabOfItems[i].length != 4) {
            return false
        } 
    }

    for (let i = 0; i < tabOfAllField.length; i++) {
        if(tabOfAllField[i].length === 0) {
            return false
        }
    }
    return true
}

exports.clientsIds = (path) => {

  const fileContent = fs.readFileSync(path, 'utf8')

  const tabOfAllLines = fileContent.split(/[\n]/);
  const clientIds = [];

  for (let i = 0; i<tabOfAllLines.length; i++) {
    clientIds.push(tabOfAllLines[i].split(';')[0]);
  }

  clientIds.forEach(id => {
    if (! isClientUnique(id)){
      return false;
    }
  })
  return true;
}


const isClientUnique = async (clientId) => {

  const isUnique = await Client.find({clientId});
  console.log(isUnique);
  if (isUnique.length > 0) {
    return false;
  }

  return true;

}

/* exports.isClientUniqueExported = async (clientId) => {

  const isUnique = await Client.find({clientId}).exec();

  if (isUnique.length > 0) {
    return false;
  }

  return true;

} */
