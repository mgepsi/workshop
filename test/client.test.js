const client = require('./client.js');

test("TestExistence01", () => {
    expect(client.isFile('test/client.txt')).toBeTruthy();
});

test("TestExistence02", () => {
    expect(client.isFile('test/client_nonExistant.txt')).toBeFalsy();
});

test("TestIntégrité01", () => {
    expect(client.isFileEmpty('test/client.txt')).not.toBe('');
});

test("TestIntégrité02", () => {
    expect(client.isFileEmpty('test/client_vide.txt')).toBeTruthy();
});

test("TestNomFichier01", () => {
    expect(client.isFileNamedProperly('test/client.txt')).toBeTruthy();
});

test("TestNomFichier02", () => {
    expect(client.isFileNamedProperly('test/client_fauxNom.txt')).toBeFalsy();
});

test("TestIntégritéDonnées01", () => {
    expect(client.isDataCorrect('test/client.txt')).toBeTruthy();
});

test("TestIntégritéDonnées02", () => {
    expect(client.isDataCorrect('test/client_errone.txt')).toBeFalsy();
});

/*test("TestUnicitéId01", () => {
    expect(client.clientsIds('test/client.txt')).toBeTruthy();
});*/

/* test("TestUnicitéId02", () => {
    expect(client.isClientUniqueExported('09A')).toBeFalsy();
}); */
