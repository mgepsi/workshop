const mongoose = require ('mongoose');

const clientSchema = mongoose.Schema({
  clientId: {
    type: String,
    required: true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Client', clientSchema);