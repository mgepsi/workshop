const Client = require('../models/client.model');

exports.createClient = (req, res, next) => {

  const client = new Client({...req.body});

  client.save()
    .then(() => res.status(201).json({ message : 'Client créé avec succès !'}))
    .catch( error => res.status(400).json({ error }));
}